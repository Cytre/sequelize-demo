# TypeScript Scaffolding
TypeScript scaffolding project to use a baseline template for new
TypeScript projects. Template includes configuration for TypeScript,
Jest, ESLint, and Prettier.

The `src` and `test` directories include example files and directories
to demonstrate the configuration. Feel free to make adjustments for your
own project as needed.
