import { config as dotenvConfig } from 'dotenv';
import { resolve } from 'path';

export default class Settings {
  [index: string]: unknown;

  public constructor() {
    const parsedEnv = dotenvConfig({ path: resolve(__dirname, '../.env') });
    const config = parsedEnv.parsed;

    // @ts-ignore
    Object.keys(config).forEach((key) => {
      if (config !== undefined) {
        this[key.toLowerCase()] = config[key];
      }
    });
  }
}
