export type Config = {
  db_host: string;
  db_user: string;
  db_password: string;
  db_name: string;
};
