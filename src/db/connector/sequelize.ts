import Settings from '@/settings';
import { Sequelize } from 'sequelize';

export default class SequelizeConnector {
  private static client: Sequelize;

  public static initialize() {
    if (!this.client) {
      const { db_user, db_password, db_host, db_name } = new Settings();

      this.client = new Sequelize(`postgres://${db_user}:${db_password}@${db_host}:5432/${db_name}`, {
        logging: false,
      });
    }

    return this.client;
  }
}
