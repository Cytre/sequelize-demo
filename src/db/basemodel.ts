import { CreationOptional, InferAttributes, InferCreationAttributes, Model, Sequelize } from 'sequelize';
import SequelizeConnector from '@/db/connector/sequelize';

export const DbConnector: Sequelize = SequelizeConnector.initialize();

class BaseModel extends Model<InferAttributes<BaseModel>, InferCreationAttributes<BaseModel>> {
  declare account_id: string;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  public static async closeDb() {
    await DbConnector.close();
  }
}

export default BaseModel;
