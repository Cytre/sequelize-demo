import { Association, CreationOptional, DataTypes, HasManyGetAssociationsMixin, NonAttribute } from 'sequelize';
import BaseModel, { DbConnector } from '@/db/basemodel';
import Book from '@/models/library/Book';
import Tag from '@/models/Tag';

class Author extends BaseModel {
  declare id: CreationOptional<string>;
  declare first_name: string;
  declare last_name: string;

  declare getBooks: HasManyGetAssociationsMixin<Book[]>;
  declare getTags: HasManyGetAssociationsMixin<Tag[]>;

  declare books?: NonAttribute<Book[]>;
  declare tags?: NonAttribute<Tag[]>;

  declare static associations: {
    books: Association<Author, Book>;
    tags: Association<Author, Tag>;
  };
}

Author.init(
  {
    // @ts-ignore
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    first_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    last_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    account_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: 'author',
    sequelize: DbConnector,
  },
);

Author.hasMany(Book, {
  sourceKey: 'id',
  foreignKey: 'author_id',
  as: 'books',
});

Author.hasMany(Tag, {
  foreignKey: 'item_id',
  as: 'tags',
});

export default Author;
