import {
  Association,
  CreationOptional,
  DataTypes,
  ForeignKey, HasManyGetAssociationsMixin,
  HasOneGetAssociationMixin,
  NonAttribute
} from "sequelize";
import BaseModel, { DbConnector } from '@/db/basemodel';
import Author from '@/models/Author';
import Tag from '@/models/Tag';

class Book extends BaseModel {
  declare id: CreationOptional<string>;
  declare title: string;
  declare isbn: number;
  declare author_id: ForeignKey<string>;

  declare getAuthors: HasOneGetAssociationMixin<Author[]>;
  declare getTags: HasManyGetAssociationsMixin<Tag[]>;

  declare authors?: NonAttribute<Author[]>;
  declare tags?: NonAttribute<Tag[]>;

  declare static associations: {
    authors: Association<Book, Author>;
    tags: Association<Book, Tag>;
  };
}

Book.init(
  {
    // @ts-ignore
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    isbn: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    author_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    },
    account_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: 'book',
    sequelize: DbConnector,
  },
);

// Book.hasMany(Author, {
//   foreignKey: 'id',
//   as: 'authors',
// });
//
// Book.hasMany(Tag, {
//   foreignKey: 'item_id',
//   as: 'tag',
// });

export default Book;
