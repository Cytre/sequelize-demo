import { CreationOptional, DataTypes, ForeignKey } from 'sequelize';
import BaseModel, { DbConnector } from '@/db/basemodel';

class Tag extends BaseModel {
  declare id: CreationOptional<number>;
  declare name: string;
  declare type: DataTypes.EnumDataType<string>;
  declare item_id: ForeignKey<string>;
}

Tag.init(
  {
    // @ts-ignore
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    item_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: 'tag',
    sequelize: DbConnector,
  },
);

export default Tag;
