import Author from '@/models/Author';
import Book from '@/models/library/Book';
import Tag from "@/models/Tag";

const user = async (): Promise<string> => {
  let author = null;
  let tags = null;

  try {
    author = await Author.findOne({
      where: {
        // @ts-ignore
        first_name: 'Kevin',
      },
      include: [
        { model: Book, as: 'books' },
        // { model: Tag, as: 'tags' },
      ],
    });

    if (author !== null) {
      tags = await author.getTags();
      author = author.toJSON();
      author.tags = tags;
    }
  } catch (e) {
    console.log(e);
  }/* finally {
    await Author.closeDb();
  } */

  return JSON.stringify(author, null, 2);
};

const users = async (): Promise<string> => {
  let authors = [];

  try {
    const authorModel = await Author.findAll({
      include: [
        { model: Book, as: 'books' },
        // { model: Tag, as: 'tags' },
      ],
    });

    for (const author of authorModel) {
      const tags = await author.getTags();
      const tmpAuthor = author.toJSON();
      tmpAuthor.tags = tags;
      authors.push(tmpAuthor);
    }
  } catch (e) {
    console.log(e);
  }/* finally {
    await Author.closeDb();
  } */

  return JSON.stringify(authors, null, 2);
};

void user().then((result) => {
  console.log(result);
});

void users().then(async (result) => {
  console.log(result);
  await Author.closeDb();
});
